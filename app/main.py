from flask import Flask, request, jsonify
import tensorflow as tf
import numpy as np
import cv2

import os

app = Flask(__name__)
model = tf.keras.models.load_model("model.h5")

os.mkdir("./cache")
lat_file = open("./coords/lat.txt", mode="r")
lon_file = open("./coords/lon.txt", mode="r")
lats = lat_file.readlines()
lat_file.close()
lons = lon_file.readlines()
lon_file.close()

lat_means = {int(line.split("->")[0]):float(line.split("->")[1]) for line in lats}
lon_means = {int(line.split("->")[0]):float(line.split("->")[1]) for line in lons}

@app.route("/predict", methods = ["POST"])
def predict():
    img = request.files["img"]
    filename = img.filename
    img.save("./cache/" + filename)
    img = cv2.imread("./cache/" + filename)
    os.remove("./cache/" + filename)
    predictions = model.predict(np.reshape(img, (1, 640, 640, 3)) / 255.0)
    predictions = predictions[0][0]
    lat = lat_means[np.argmax(predictions)]
    lon = lon_means[np.argmax(predictions)]
    return jsonify(
        latitude=lat,
        longitude=lon
    )
